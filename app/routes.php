<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/*Route::get('admin', function(){
	if(Auth::check()){
		return Redirect::to('admin/user/visualdata');
	} else {
		return Redirect::to('login');
	}
});
*/


//For Login User
Route::get('login', 'SessionController@create');
Route::resource('sessions', 'SessionController');

//Route for Backend
Route::resource('admin/users', 'UserController');
Route::get('admin/users/destroy/{id}', 'UserController@destroy');
Route::resource('admin/categories', 'CategoryController');
Route::get('admin/categories/destroy/{id}', 'CategoryController@destroy');
Route::resource('admin/reports', 'ReportController');
Route::get('admin/reports/destroy/{id}', 'ReportController@destroy');
Route::resource('admin/achievements', 'AchievementController');
Route::get('admin/achievements/destroy/{id}', 'AchievementController@destroy');
Route::resource('admin/userachievements', 'UserachievementController');
Route::get('admin/userachievements/destroy/{id}', 'UserachievementController@destroy');


//Route for Frontend
Route::get('dinas/realtimemaps','VisualdataController@getIndex');
Route::get('dinas/visualdata','VisualdataController@getCharts');
Route::get('dinas/tables/{category}','VisualdataController@getTables');
Route::post('updatestatus/{id}','FReportController@updateStatus');

//Route for Android Side Request
Route::post('addnewreport','ReportController@postAddreport');
Route::post('getprofile','UserController@postGetprofile');
Route::post('getleaderboard','UserController@postGetleaderboard');

HTML::macro('nav_link', function($routes, $text) {

	$link = explode('/', Request::path());
	
	foreach($routes as $route){
		if ($link[1] == $route){
			$active = "class = 'active'";
			break;
		} else
			$active = '';
	}

  	return '<li '.$active.'><a href="'.url('admin/'.$route.'').'"><i class="fa fa-folder"></i><span>'.$text.'</span></a></li>';
});

HTML::macro('nav_dinas', function($routes, $text) {

	$link = explode('/', Request::path());
	
	foreach($routes as $route){
		if ($link[1] == $route){
			$active = "class = 'active'";
			break;
		} else
			$active = '';
	}

  	return '<li '.$active.'><a href="'.url('dinas/'.$route.'').'"><i class="fa fa-folder"></i><span>'.$text.'</span></a></li>';
});

HTML::macro('nav_menu', function($route, $text) {
	if(Request::is("*/".$route."/*") || Request::is("*/".$route)){
		$active = "class = 'active'";
	} else {
		$active = '';
	}
	/*
	$link = explode('/', Request::path());
	if ($link[count($link)-2].'/'.$link[count($link)-1] == $route)
		$active = "class = 'active'";
	else
		$active = '';
	*/
  	return '<li '.$active.'><a href="'.url('admin/'.$route).'">'.$text.'</a></li>';
});

Route::get('/',function(){
	return Redirect::to('admin/users');
});

Route::get('/boooootogermany',function(){
	return View::make('booooo.index');
});