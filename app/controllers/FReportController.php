<?php

class FReportController extends BaseController {

   public function updateStatus($id){
        $input = Input::all();
        $report = Report::find($id);
        $report->status = Input::get('newstatus');
        $report->save();
        
        return Redirect::back();
   }
}