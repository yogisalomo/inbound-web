<?php

class UserController extends GenericController {

    // public $task;
    public $names = 'users';
    public $name = 'user';
    public $location = 'backend.admin';
    public $scope = 'admin';

    public function __construct(User $user){
        $this->db = $user;
    }

    public function postGetprofile(){
        $fbid = Input::get('FacebookID');
        $usercount = User::where('facebook_id','=',$fbid)->count();
        if($usercount==0){
            //Register the User to Application Database
            $user = new User();
            $user->username = Input::get('name');
            $user->name = Input::get('name');
            $user->facebook_id = $fbid;
            $user->point = 0;
            $user->role_id = 3;
            $user->is_active = 1;
            
            $user->save();
        }

        $user = User::where('facebook_id','=',$fbid)->firstOrFail();

        return json_encode($user);
    }

    public function postGetleaderboard(){
        
        $userlist = User::orderBy('point','desc')->get();
        
        return $userlist;
    }

    public function getAndroidLogin(){
        $fbid = Input::get('FacebookID');
        $usercount = User::where('facebook_id','=',$fbid)->count();

        if($usercount==0){
            //Register the User to Application Database
            $user = new User();
            $user->username = Input::get('username');
            $user->name = Input::get('name');
            $user->facebook_id = $fbid;
            $user->email = Input::get('email');
            $user->point = 0;
            $user->role_id = 3;
            $user->is_active = 1;
            
            $user->save();
        }
        else {
            //Return the Information of the user
            $user = User::where('facebook_id','=',$fbid)->firstOrFail();
        }

        return $user;
    }
    /*
    public function store()
    {
        $input = Input::all();
        if (! $this->db->fill($input)->isValid()) {
            return Redirect::back()->withInput()->withErrors($this->db->errors);
        }
        $upload = $this->fileUpload('file_document', 'filename', ['pdf', 'doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx', 'jpg', 'jpeg', 'png', 'gif', 'svg'], $this->db);

        if ($upload === true) {
            $this->db->save();
            if (Input::get('redirect'))
                return Redirect::to(Input::get('redirect'))->with('alert', studly_case($this->name).' created');
            else 
                return Redirect::route($this->getScope(). $this->names.'.index')->with('alert', studly_case($this->name).' created');
        } else {
            return $upload;
        }
    }

    public function update($id)
    {
        $input = Input::all();
        $name = $this->name;
        $$name = $this->db->find($id);

        if (! $$name->fill($input)->isValid()) {
            return Redirect::back()->withInput()->withErrors($$name->errors);
        }

        $upload = $this->fileUpload('file_document', 'filename', ['pdf', 'doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx', 'jpg', 'jpeg', 'png', 'gif', 'svg'], $$name);

        if ($upload === true)  {
            $$name->save();
            return Redirect::route($this->getScope(). $this->names.'.index');
        } else {
            return $upload;
        }
    }
    */
}