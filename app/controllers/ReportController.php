<?php

class ReportController extends GenericController {

    // public $task;
    public $names = 'reports';
    public $name = 'report';
    public $location = 'backend.admin';
    public $scope = 'admin';

    public function __construct(Report $report){
        $this->db = $report;
    }
/*
    public function store()
    {
        $input = Input::all();
        if (! $this->db->fill($input)->isValid()) {
            return Redirect::back()->withInput()->withErrors($this->db->errors);
        }
        $upload = $this->fileUpload('file_document', 'filename', ['pdf', 'doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx', 'jpg', 'jpeg', 'png', 'gif', 'svg'], $this->db);

        if ($upload === true) {
            $this->db->save();
            if (Input::get('redirect'))
                return Redirect::to(Input::get('redirect'))->with('alert', studly_case($this->name).' created');
            else 
                return Redirect::route($this->getScope(). $this->names.'.index')->with('alert', studly_case($this->name).' created');
        } else {
            return $upload;
        }
    }

    public function update($id)
    {
        $input = Input::all();
        $name = $this->name;
        $$name = $this->db->find($id);

        if (! $$name->fill($input)->isValid()) {
            return Redirect::back()->withInput()->withErrors($$name->errors);
        }

        $upload = $this->fileUpload('file_document', 'filename', ['pdf', 'doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx', 'jpg', 'jpeg', 'png', 'gif', 'svg'], $$name);

        if ($upload === true)  {
            $$name->save();
            return Redirect::route($this->getScope(). $this->names.'.index');
        } else {
            return $upload;
        }
    }
    */

    public function postAddreport(){
        $file_path = public_path()."/report_photos/"      ;           
        $tStamp = date_create();
        $timestamp =  date_format($tStamp, 'Ymd-Hms');
        $file_path = $timestamp . "_";
        $file_path = $file_path . basename($_FILES['uploaded_file']['name']);
        if(move_uploaded_file($_FILES['uploaded_file']['tmp_name'], $file_path)) {
            echo "Report Success";
        } else{
            echo "Fail to Send Report";
        }
        
        $report = new Report();
        $report->user_id = 1; //This is Dummy for Prototype
        $report->picture = $timestamp . "_".basename($_FILES['uploaded_file']['name']);
        $report->category_id = $_POST["category_id"];
        $report->description = $_POST["description"];
        $report->latitude = $_POST["latitude"];
        $report->longitude = $_POST["longitude"];
        $report->status = 0;
        $report->save();
    }
}