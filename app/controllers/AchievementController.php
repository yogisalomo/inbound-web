<?php

class AchievementController extends GenericController {

    // public $task;
    public $names = 'achievements';
    public $name = 'achievement';
    public $location = 'backend.admin';
    public $scope = 'admin';

    public function __construct(Achievement $achievement){
        $this->db = $achievement;
    }
    /*
    public function store()
    {
        $input = Input::all();
        if (! $this->db->fill($input)->isValid()) {
            return Redirect::back()->withInput()->withErrors($this->db->errors);
        }
        $upload = $this->fileUpload('file_document', 'filename', ['pdf', 'doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx', 'jpg', 'jpeg', 'png', 'gif', 'svg'], $this->db);

        if ($upload === true) {
            $this->db->save();
            if (Input::get('redirect'))
                return Redirect::to(Input::get('redirect'))->with('alert', studly_case($this->name).' created');
            else 
                return Redirect::route($this->getScope(). $this->names.'.index')->with('alert', studly_case($this->name).' created');
        } else {
            return $upload;
        }
    }

    public function update($id)
    {
        $input = Input::all();
        $name = $this->name;
        $$name = $this->db->find($id);

        if (! $$name->fill($input)->isValid()) {
            return Redirect::back()->withInput()->withErrors($$name->errors);
        }

        $upload = $this->fileUpload('file_document', 'filename', ['pdf', 'doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx', 'jpg', 'jpeg', 'png', 'gif', 'svg'], $$name);

        if ($upload === true)  {
            $$name->save();
            return Redirect::route($this->getScope(). $this->names.'.index');
        } else {
            return $upload;
        }
    }
    */
}