<?php 

class VisualdataController extends BaseController {

    public function getIndex()
    {
        $reports = Report::where('status','<',4)->get();
        return View::make('frontend.index')->with(array('reports'=>$reports));
    }

    public function getCharts(){
        
        $reports = Report::all();
        $monthchartdata = array(12);
        for($i = 0; $i<12;$i++){
            $monthchartdata[$i]=0;
        }
        foreach ($reports as $report) {
            $date = date_parse_from_format("Y-m-d",$report->created_at);
            $monthchartdata[$date["month"]-1]+=1;
        }

        //return $monthchartdata;
        return View::make('frontend.chart')->with(array('monthchartdata'=>$monthchartdata));;
    }

    public function getTables($category){
        if($category=='user'){
            
            $citizenusers = User::whereNull('nip')->get();
            $dinasusers = User::whereNotNull('nip')->get();
            return View::make('frontend.tableuser')->with(array('citizenusers'=>$citizenusers,'dinasusers'=>$dinasusers));;
        }
        else if($category=='report'){
            $reports = Report::all();
            return View::make('frontend.tablereport')->with(array('reports'=>$reports));;
        }
        else{
            //Category is undefined, assume Report
            $reports = Report::all();
            return View::make('frontend.tablereport')->with(array('reports'=>$reports));;
        }
    }
}

?>