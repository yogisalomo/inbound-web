@extends('layouts.frontend')
@section('content')		
	@include('layouts.tabel_nav')
	<h2>Citizen Users</h2>
	<table id="user" class="table table-striped table-bordered table-hover table-condensed" cellspacing="0" width="100%">
	  <thead>
		  <tr>
		  	<td><b>ID</b></td>
		  	<td><b>Username</b></td>
			<td><b>Point</b></td>
		  	<td><b>Name</b></td>
		  	<td><b>Address</b></td>
		  	<td><b>Member Since</b></td>
		  	<td><b>Flag</b></td>
		  	<td><b>Is Active</b></td>
		  	<td><b>Last Visit</b></td>
		  	<td><b>Flag</b></td>
		  </tr>
	  </thead>
	  <tbody>
	  	@foreach($citizenusers as $citizenuser)
		  <tr>
		  	<td><a href="{{url('admin/citizenuser/view/'.$citizenuser->id)}}">{{$citizenuser->id}}</a></td>
		  	<td><a href="{{url('admin/citizenuser/view/'.$citizenuser->id)}}">{{$citizenuser->username}}</a></td>
		  	<td>{{$citizenuser->point}}</td>
		  	<td>{{$citizenuser->name}}</td>
		  	<td>{{$citizenuser->address_street}}</td>
		  	<td>{{$citizenuser->registration_date}}</td>
		  	<td>{{$citizenuser->flag}}</td>
		  	<td>{{$citizenuser->is_active}}</td>
		  	<td>{{$citizenuser->last_visit}}</td>
		  	<td><a href="{{url('admin/citizenusers/flag/'.$citizenuser->id)}}" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-flag"></span></a></td>
		  </tr>
		@endforeach
	  </tbody>
	</table>

	<h2>Pegawai Dinas</h2>
	<table id="userdinas" class="table table-striped table-bordered table-hover table-condensed" cellspacing="0" width="100%">
	  <thead>
		  <tr>
		  	<td><b>ID</b></td>
		  	<td><b>Name</b></td>
		  	<td><b>NIP</b></td>
		  	<td><b>Address</b></td>
		  	<td><b>Member Since</b></td>
		  	<td><b>Flag</b></td>
		  	<td><b>Is Active</b></td>
		  	<td><b>Last Visit</b></td>
		  	<td><b>Menu</b></td>
		  </tr>
	  </thead>
	  <tbody>
	  	@foreach($dinasusers as $dinasuser)
		  <tr>
		  	<td><a href="{{url('admin/dinasuser/view/'.$dinasuser->id)}}">{{$dinasuser->id}}</a></td>
		  	<td>{{$dinasuser->name}}</td>
		  	<td>{{$dinasuser->nip}}</td>
		  	<td>{{$dinasuser->address_street}}</td>
		  	<td>{{$dinasuser->registration_date}}</td>
		  	<td>{{$dinasuser->flag}}</td>
		  	<td>{{$dinasuser->is_active}}</td>
		  	<td>{{$dinasuser->last_visit}}</td>
		  	<td><a href="{{url('admin/dinasusers/flag/'.$citizenuser->id)}}" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-flag"></span></a></td>
		  </tr>
		@endforeach
	  </tbody>
	</table>

@stop

@section('page_script')
<script>	
	$(document).ready(function() {
    $('#user').dataTable();
    $('#userdinas').dataTable();
} );
</script>
@stop