<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>InBound | Infrastruktur Bandung</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="{{asset('css/ionicons.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="{{asset('css/AdminLTE.css')}}" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="{{asset('css/dataTables.bootstrap.css')}}">

        <link href="{{asset('css/bootstrap-datetimepicker.css')}}" rel="stylesheet">
        <script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
        <script src="{{asset('js/moment.js')}}"></script>
        <script src="{{asset('js/bootstrap-datetimepicker.js')}}"></script>
        <script src="{{asset('amcharts/amcharts.js')}}"></script>
        <script src="{{asset('amcharts/serial.js')}}"></script>
        <script type="text/javascript">
            var chart;

            var chartData = [
                {
                    "months": "January",
                    "reports": {{$monthchartdata[0]}}
                },
                {
                    "months": "February",
                    "reports": {{$monthchartdata[1]}}
                },
                {
                    "months": "March",
                    "reports": {{$monthchartdata[2]}}
                },
                {
                    "months": "April",
                    "reports": {{$monthchartdata[3]}}
                },
                {
                    "months": "May",
                    "reports": {{$monthchartdata[4]}}
                },
                {
                    "months": "June",
                    "reports": {{$monthchartdata[5]}}
                },
                {
                    "months": "July",
                    "reports": {{$monthchartdata[6]}}
                },
                {
                    "months": "August",
                    "reports": {{$monthchartdata[7]}}
                },
                {
                    "months": "September",
                    "reports": {{$monthchartdata[8]}}
                },
                {
                    "months": "October",
                    "reports": {{$monthchartdata[9]}}
                },
                {
                    "months": "November",
                    "reports": {{$monthchartdata[10]}}
                },
                {
                    "months": "December",
                    "reports": {{$monthchartdata[11]}}
                }
            ];


            AmCharts.ready(function () {
                // SERIAL CHART
                chart = new AmCharts.AmSerialChart();
                chart.dataProvider = chartData;
                chart.categoryField = "months";
                chart.startDuration = 1;

                // AXES
                // category
                var categoryAxis = chart.categoryAxis;
                categoryAxis.labelRotation = 90;
                categoryAxis.gridPosition = "start";

                // value
                // in case you don't want to change default settings of value axis,
                // you don't need to create it, as one value axis is created automatically.

                // GRAPH
                var graph = new AmCharts.AmGraph();
                graph.valueField = "reports";
                graph.balloonText = "[[category]]: <b>[[value]]</b>";
                graph.type = "column";
                graph.lineAlpha = 0;
                graph.fillAlphas = 0.8;
                chart.addGraph(graph);

                // CURSOR
                var chartCursor = new AmCharts.ChartCursor();
                chartCursor.cursorAlpha = 0;
                chartCursor.zoomable = false;
                chartCursor.categoryBalloonEnabled = false;
                chart.addChartCursor(chartCursor);

                chart.creditsPosition = "top-right";

                chart.write("chartdiv");
            });
        </script>
    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="../../index.html" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                InBound
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="{{asset('img/avatar3.png')}}" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Hello, Admin</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input class="form-control" id="typeahead" type="text" data-provide="typeahead" autocomplete="off" placeholder="Search..."/>
                            <span class="input-group-btn">

                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu"> 
                        {{ HTML::nav_dinas(array("realtimemaps"), 'Real Time Maps' ) }}
                        {{ HTML::nav_dinas(array("visualdata"), 'Visual Data' ) }}
                        {{ HTML::nav_dinas(array("tables"), 'Tables' ) }}
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->

                <!-- Main content -->
                <section class="content">
                    <h3>Jumlah Laporan Per Bulan</h3>
                    <div id="chartdiv" style="width: 100%; height: 400px;"></div>
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- Bootstrap -->
        <script src="{{asset('js/bootstrap.min.js')}}" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="{{asset('js/AdminLTE/app.js')}}" type="text/javascript"></script>
        <!-- AdminLTE for demo purposes -->

        <script type="text/javascript" src="{{asset('js/tinymce.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/jquery-ui.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/jquery.dataTables.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/jquery.jeditable.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/jquery.dataTables.editable.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/jquery.validate.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/scripts.js')}}"></script>
        <script type="text/javascript" src="{{asset('highcharts/js/highcharts.js')}}"></script>
        <script type="text/javascript" src="{{asset('highcharts/js/modules/funnel.js')}}"></script>
        <script type="text/javascript" src="{{asset('highcharts/js/modules/exporting.js')}}"></script>
        <script type="text/javascript" src="{{asset('tabletools/js/dataTables.tableTools.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/dataTables.bootstrap.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/bootstrap3-typeahead.min.js')}}"></script>
        @yield('page_script')
        <script>
            $('#typeahead').typeahead({
                    minLength:2,
                    updater: function (item) {
                        var html = $.parseHTML(item);
                        console.log(html);
                        window.location.href = html[0].href;
                    },
                    source: function (query, process) {
                        return $.getJSON(
                            '{{url('search')}}/'+query,
                            {},
                            function (data) {
                                return process(data);
                            }
                        );
                    }   
                });
        </script>
        <script type="text/javascript">
            $('#datepick1').datetimepicker({
                pickTime: false
            });
        </script>
    </body>
</html>
