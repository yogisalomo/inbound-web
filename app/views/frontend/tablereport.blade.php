@extends('layouts.frontend')
@section('content')		
	@include('layouts.tabel_nav')
	<h2>Reports</h2>
	<table id="report" class="table table-striped table-bordered table-hover table-condensed" cellspacing="0" width="100%">
	  <thead>
		  <tr>
		  	<td><b>ID</b></td>
		  	<td><b>Category</b></td>
		  	<td><b>Image</b></td>
		  	<td><b>Sent By</b></td>
		  	<td><b>Description</b></td>
		  	<td><b>Status</b></td>
		  </tr>
	  </thead>
	  <tbody>
	  	@foreach($reports as $report)
		  <tr>
		  	<td>{{$report->id}}</td>
		  	<td>{{Category::find($report->category_id)->name}}</td>
		  	<td><a href="{{url('admin/report/view/'.$report->id)}}">
		  		<img src="{{asset('report_photos/'.$report->picture)}}" width="200px" height="auto"/></a></td>
		  	<td>{{User::find($report->user_id)->name}}</td>
		  	<td>{{$report->description}}</td>
		  	<td>{{$report->getStatusName()}}</td>
		  	<td>{{$report->last_visit}}</td>
		  </tr>
		@endforeach
	  </tbody>
	</table>

@stop

@section('page_script')
<script>	
	$(document).ready(function() {
    $('#report').dataTable();
} );
</script>
@stop