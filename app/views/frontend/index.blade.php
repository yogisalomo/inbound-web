<!DOCTYPE html>
<html>
  <head>
    <title>InBound | Infrastruktur Bandung</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- font Awesome -->
    <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="{{asset('css/ionicons.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{asset('css/AdminLTE.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/dataTables.bootstrap.css')}}">

    <link href="{{asset('css/bootstrap-datetimepicker.css')}}" rel="stylesheet">
    <script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/moment.js')}}"></script>
    <script src="{{asset('js/bootstrap-datetimepicker.js')}}"></script>
    <style>
      html, body, #map-canvas {
        height: 100%;
        margin: 0px;
        padding: 0px
      }
    </style>
    <script type="text/javascript"
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDNxaY9Ne4K6v9R7TDO3fZ4ol-p6dAW05A">
    </script>
    <script>
        var map;
        function initialize() {
          var mapOptions = {
            zoom: 14,
            center: new google.maps.LatLng(-6.9147, 107.6098)
          };
          map = new google.maps.Map(document.getElementById('map-canvas'),
              mapOptions);
            // Sender ID, Description, Picture File Name, Latitude, Longitude
            var locations = [
                @foreach($reports as $report)
                    ["{{User::find($report->user_id)->name}}",'{{$report->description}}','{{asset("report_photos/".$report->picture)}}',{{$report->latitude}},{{$report->longitude}},{{$report->id}},"{{$report->getStatusName()}}"],
                @endforeach
            ];
          
            for (var i = 0; i < locations.length; i++) {
              
                  var infowindow = new google.maps.InfoWindow({
                      maxWidth: 320 
                  });

                  var marker = new google.maps.Marker({
                      position: new google.maps.LatLng(locations[i][3],locations[i][4]),
                      map: map,
                      title: "By "+locations[i][0]
                  });
                  var updateStatus = "updatestatus/"+locations[i][5];
                  google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                      infowindow.setContent('<div id="content">'+
                  '<div id="siteNotice">'+
                  '</div>'+
                    '<h1 id="firstHeading" class="firstHeading">By '+locations[i][0]+'</h1>'+
                    '<div id="bodyContent">'+
                        '<img src="'+locations[i][2]+'"/>'+
                        '<form method="POST" action="{{url("updatestatus")}}/'+locations[i][5]+'">'+
                            '<label>Oleh:'+locations[i][0]+'</label><br>'+
                            '<label>'+locations[i][1]+'</label><br>'+
                            '<label>Current Status : '+locations[i][6]+'</label><br>'+
                            '{{Form::label("Update Status Report :")}}'+
                            '{{Form::select("newstatus",array(0=>"Belum direspon",1=>"Ditunda",2=>"Sedang Dikerjakan", 3=>"Selesai", 4=>"Tidak Layak",5=>"SPAM"),["class"=>"form_control"])}}<br>'+
                            '{{Form::submit("Update", array("class"=>"btn btn-info"))}}'+
                        '{{Form::close()}}'+
                    '</div>'+
                  '</div>');
                      infowindow.open(map, marker);
                    }
                  })(marker, i));
            }
        }

        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
  </head>
  <body>
    <header class="header">
        <a href="{{url('/')}}" class="logo">
            <!-- Add the class icon to your logo image or logo icon to add the margining -->
            InBound
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
        </nav>
    </header>
    <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="{{asset('img/avatar3.png')}}" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Hello, Admin</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input class="form-control" id="typeahead" type="text" data-provide="typeahead" autocomplete="off" placeholder="Search..."/>
                            <span class="input-group-btn">

                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu"> 
                        {{ HTML::nav_dinas(array("realtimemaps"), 'Real Time Maps' ) }}
                        {{ HTML::nav_dinas(array("visualdata"), 'Visual Data' ) }}
                        {{ HTML::nav_dinas(array("tables/report"), 'Tables' ) }}
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <h1>
                        Real Time Map
                    </h1>
                </div>

                <!-- Main content -->
                <div class="content" style="height:800px">
                    <div id="map-canvas"></div>
                </div><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
        <script src="{{asset('js/bootstrap.min.js')}}" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="{{asset('js/AdminLTE/app.js')}}" type="text/javascript"></script>
        <!-- AdminLTE for demo purposes -->

        <script type="text/javascript" src="{{asset('js/tinymce.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/jquery-ui.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/jquery.dataTables.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/jquery.jeditable.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/jquery.dataTables.editable.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/jquery.validate.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/scripts.js')}}"></script>
        <script type="text/javascript" src="{{asset('highcharts/js/highcharts.js')}}"></script>
        <script type="text/javascript" src="{{asset('highcharts/js/modules/funnel.js')}}"></script>
        <script type="text/javascript" src="{{asset('highcharts/js/modules/exporting.js')}}"></script>
        <script type="text/javascript" src="{{asset('tabletools/js/dataTables.tableTools.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/dataTables.bootstrap.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/bootstrap3-typeahead.min.js')}}"></script>
  </body>
</html>