<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>InBound | Dashboard</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="{{asset('css/ionicons.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="{{asset('css/AdminLTE.css')}}" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="{{asset('css/dataTables.bootstrap.css')}}">

        <link href="{{asset('css/bootstrap-datetimepicker.css')}}" rel="stylesheet">
        <script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
        <script src="{{asset('js/moment.js')}}"></script>
        <script src="{{asset('js/bootstrap-datetimepicker.js')}}"></script>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="{{url('/')}}" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                InBound
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="{{asset('img/avatar3.png')}}" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Hello, Admin</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input class="form-control" id="typeahead" type="text" data-provide="typeahead" autocomplete="off" placeholder="Search..."/>
                            <span class="input-group-btn">

                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu"> 
                        {{ HTML::nav_dinas(array("realtimemaps"), 'Real Time Maps' ) }}
                        {{ HTML::nav_link(array("users"), 'User' ) }}
                        {{ HTML::nav_link(array("categories"), 'Category' ) }}
                        {{ HTML::nav_link(array("reports"), 'Report' ) }}
                        {{ HTML::nav_link(array("achievements"), 'Achievement' ) }}
                        {{ HTML::nav_link(array("userachievements"), 'User Achievement' ) }}
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content">
                    @yield('content')

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- Bootstrap -->
        <script src="{{asset('js/bootstrap.min.js')}}" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="{{asset('js/AdminLTE/app.js')}}" type="text/javascript"></script>
        <!-- AdminLTE for demo purposes -->

        <script type="text/javascript" src="{{asset('js/tinymce.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/jquery-ui.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/jquery.dataTables.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/jquery.jeditable.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/jquery.dataTables.editable.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/jquery.validate.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/scripts.js')}}"></script>
        <script type="text/javascript" src="{{asset('highcharts/js/highcharts.js')}}"></script>
        <script type="text/javascript" src="{{asset('highcharts/js/modules/funnel.js')}}"></script>
        <script type="text/javascript" src="{{asset('highcharts/js/modules/exporting.js')}}"></script>
        <script type="text/javascript" src="{{asset('tabletools/js/dataTables.tableTools.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/dataTables.bootstrap.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/bootstrap3-typeahead.min.js')}}"></script>
        @yield('page_script')
        <script>
            $('#typeahead').typeahead({
                    minLength:2,
                    updater: function (item) {
                        var html = $.parseHTML(item);
                        console.log(html);
                        window.location.href = html[0].href;
                    },
                    source: function (query, process) {
                        return $.getJSON(
                            '{{url('search')}}/'+query,
                            {},
                            function (data) {
                                return process(data);
                            }
                        );
                    }   
                });
        </script>
        <script type="text/javascript">
            $('#datepick1').datetimepicker({
                pickTime: false
            });
        </script>
    </body>
</html>
