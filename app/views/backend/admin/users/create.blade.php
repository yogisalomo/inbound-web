@extends('layouts.admin')
@section('content')		
	@include('layouts.user_nav')
	<h2>Create User</h2>
<div class="row">

    <div class="col-sm-12">
        {{Form::open(['route'=>'admin.users.store', 'enctype' => 'multipart/form-data'])}}
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Username </label>
                <div class="col-sm-9">
                    {{Form::text('username',null,array(
                            'id'=> 'username'
                    ))}}
                </div>
            <br><label id="error_username" class="col-sm-3 control-label no-padding-right" for="form-field-1" style="color:red;">{{$errors->first('username')}}</label>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Name </label>
                <div class="col-sm-9">
                    {{Form::text('name',null,array(
                            'id'=> 'name'
                    ))}}
                </div>
            <br><label id="error_username" class="col-sm-3 control-label no-padding-right" for="form-field-1" style="color:red;">{{$errors->first('username')}}</label>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> NIP </label>
                <div class="col-sm-9">
                    {{Form::text('nip',null,array(
                            'id'=> 'nip'
                    ))}}
                </div>
            <br><label id="error_username" class="col-sm-3 control-label no-padding-right" for="form-field-1" style="color:red;">{{$errors->first('username')}}</label>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Password </label>
                <div class="col-sm-9">
                    {{Form::password('password',array(
                            'id'=> 'password'
                    ))}}
                </div>
                <br><label id="error_password" class="col-sm-3 control-label no-padding-right" for="form-field-1" style="color:red;">{{$errors->first('password')}}</label>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Picture </label>
                <div class="col-sm-9">
                    {{Form::file('picture',array(
                            'id'=> 'picture'
                    ))}}
                </div>
            <br><label id="error_picture" class="col-sm-3 control-label no-padding-right" for="form-field-1" style="color:red;">{{$errors->first('picture')}}</label>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Role </label>
                <div class="col-sm-9">
                    {{Form::select('role_id',array( '0'=>'Admin', '1' =>'Walikota', '2'=>'Dinas', '3'=>'User'),'2',array(
                            'id'=> 'role_id'
                    ))}}
                </div>
            <br><label id="error_role_id" class="col-sm-3 control-label no-padding-right" for="form-field-1" style="color:red;">{{$errors->first('role_id')}}</label>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Address Street</label>
                <div class="col-sm-9">
                    {{Form::text('address_street',null,array(
                            'id'=> 'address_street'
                    ))}}
                </div>
        </div>
       

        <div class="form-group">
                            <div class="col-md-offset-3 col-md-9">
                                {{Form::submit('Simpan', array('class'=>'btn btn-info'))}}

                                <a href="/admin/user/">
                                    <button class="btn">
                                        <i class="icon-undo bigger-110"></i>
                                        Kembali
                                    </button>
                                </a>
                            </div>

                        </div>
         {{Form::close()}}
    </div>
</div>	

@stop

@section('page_script')

@stop

