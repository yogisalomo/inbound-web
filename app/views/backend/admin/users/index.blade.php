@extends('layouts.admin')
@section('content')		
	@include('layouts.user_nav')
	<h2>Users</h2>
	<table id="user" class="table table-striped table-bordered table-hover table-condensed" cellspacing="0" width="100%">
	  <thead>
		  <tr>
		  	<td><b>ID</b></td>
		  	<td><b>Username</b></td>
		  	<td><b>Name</b></td>
		  	<td><b>Address</b></td>
		  	<td><b>Member Since</b></td>
		  	<td><b>Flag</b></td>
		  	<td><b>Is Active</b></td>
		  	<td><b>Last Visit</b></td>
		  	<td><b>Menu</b></td>
		  </tr>
	  </thead>
	  <tbody>
	  	@foreach($users as $user)
		  <tr>
		  	<td><a href="{{url('admin/user/view/'.$user->id)}}">{{$user->id}}</a></td>
		  	<td><a href="{{url('admin/user/view/'.$user->id)}}">{{$user->username}}</a></td>
		  	<td>{{$user->name}}</td>
		  	<td>{{$user->address_street}}</td>
		  	<td>{{$user->email}}</td>
		  	<td>{{$user->registration_date}}</td>
		  	<td>{{$user->flag}}</td>
		  	<td>{{$user->last_visit}}</td>
		  	<td><a href="{{url('admin/users/'.$user->id.'/edit')}}" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-pencil"></span></a>&nbsp;<a href="{{url('admin/users/delete/'.$user->id)}}" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span></a></td>
		  	<td><a href="{{url('admin/users/flag/'.$user->id)}}" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-flag"></span></a></td>
		  </tr>
		@endforeach
	  </tbody>
	</table>

@stop

@section('page_script')
<script>	
	$(document).ready(function() {
    $('#user').dataTable();
} );
</script>
@stop

