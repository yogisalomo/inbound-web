@extends('layouts.admin')
@section('content')		
	<h2>User Achievements</h2>
	<table id="userachievement" class="table table-striped table-bordered table-hover table-condensed" cellspacing="0" width="100%">
	  <thead>
		  <tr>
		  	<td><b>ID</b></td>
		  	<td><b>User</b></td>
		  	<td><b>Achievement</b></td>
		  	<td><b>Menu</b></td>
		  </tr>
	  </thead>
	  <tbody>
	  	@foreach($userachievements as $userachievement)
		  <tr>
		  	<td>{{$userachievement->id}}</td>
		  	<td>{{$userachievement->user_id}}</td>
		  	<td>{{$userachievement->achievement_id}}</td>
		  	<td><a href="{{url('admin/userachievements/'.$userachievement->id.'/edit')}}" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-pencil"></span></a>&nbsp;<a href="{{url('admin/userachievements/destroy/'.$userachievement->id)}}" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span></a></td>
		  </tr>
		@endforeach
	  </tbody>
	</table>

@stop

@section('page_script')
<script>	
	$(document).ready(function() {
    $('#userachievement').dataTable();
} );
</script>
@stop

