@extends('layouts.admin')
@section('content')		
	<h2>Achievements</h2>
	<table id="achievement" class="table table-striped table-bordered table-hover table-condensed" cellspacing="0" width="100%">
	  <thead>
		  <tr>
		  	<td><b>ID</b></td>
		  	<td><b>Image</b></td>
		  	<td><b>Name</b></td>
		  	<td><b>Description</b></td>
		  	<td><b>Reports Required</b></td>
		  	<td><b>Reports Processed Required</b></td>
		  	<td><b>Menu</b></td>
		  </tr>
	  </thead>
	  <tbody>
	  	@foreach($achievements as $achievement)
		  <tr>
		  	<td><a href="{{url('admin/achievement/view/'.$achievement->id)}}">{{$achievement->id}}</a></td>
		  	<td><a href="{{url('admin/achievement/view/'.$achievement->id)}}">{{$achievement->picture}}</a></td>
		  	<td>{{$achievement->name}}</td>
		  	<td>{{$achievement->description}}</td>
		  	<td>{{$achievement->num_report}}</td>
		  	<td>{{$achievement->num_processed}}</td>
		  	<td>{{$achievement->last_visit}}</td>
		  	<td><a href="{{url('admin/achievements/'.$achievement->id.'/edit')}}" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-pencil"></span></a>&nbsp;<a href="{{url('admin/achievements/destroy/'.$achievement->id)}}" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span></a></td>
		  </tr>
		@endforeach
	  </tbody>
	</table>

@stop

@section('page_script')
<script>	
	$(document).ready(function() {
    $('#achievement').dataTable();
} );
</script>
@stop

