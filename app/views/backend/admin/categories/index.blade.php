@extends('layouts.admin')
@section('content')		
	<h2>Categories</h2>
	<table id="category" class="table table-striped table-bordered table-hover table-condensed" cellspacing="0" width="100%">
	  <thead>
		  <tr>
		  	<td><b>ID</b></td>
		  	<td><b>Image</b></td>
		  	<td><b>Name</b></td>
		  	<td><b>Description</b></td>
		  	<td><b>Menu</b></td>
		  </tr>
	  </thead>
	  <tbody>
	  	@foreach($categories as $category)
		  <tr>
		  	<td><a href="{{url('admin/category/view/'.$category->id)}}">{{$category->id}}</a></td>
		  	<td><a href="{{url('admin/category/view/'.$category->id)}}">{{$category->categoryname}}</a></td>
		  	<td>{{$category->name}}</td>
		  	<td>{{$category->address_street}}</td>
		  	<td>{{$category->email}}</td>
		  	<td>{{$category->registration_date}}</td>
		  	<td>{{$category->flag}}</td>
		  	<td>{{$category->last_visit}}</td>
		  	<td><a href="{{url('admin/categories/'.$category->id.'/edit')}}" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-pencil"></span></a>&nbsp;<a href="{{url('admin/categories/destroy/'.$category->id)}}" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span></a></td>
		  </tr>
		@endforeach
	  </tbody>
	</table>

@stop

@section('page_script')
<script>	
	$(document).ready(function() {
    $('#category').dataTable();
} );
</script>
@stop

