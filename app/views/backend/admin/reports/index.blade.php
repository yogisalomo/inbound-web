@extends('layouts.admin')
@section('content')		
	<h2>Reports</h2>
	<table id="report" class="table table-striped table-bordered table-hover table-condensed" cellspacing="0" width="100%">
	  <thead>
		  <tr>
		  	<td><b>ID</b></td>
		  	<td><b>Category</b></td>
		  	<td><b>Image</b></td>
		  	<td><b>Sent By</b></td>
		  	<td><b>Description</b></td>
		  	<td><b>Status</b></td>
		  	<td><b>Menu</b></td>
		  </tr>
	  </thead>
	  <tbody>
	  	@foreach($reports as $report)
		  <tr>
		  	<td><a href="{{url('admin/report/view/'.$report->id)}}">{{$report->id}}</a></td>
		  	<td>{{$report->category_id}}</td>
		  	<td><a href="{{url('admin/report/view/'.$report->id)}}">{{$report->picture}}</a></td>
		  	<td>{{$report->user_id}}</td>
		  	<td>{{$report->description}}</td>
		  	<td>{{$report->getStatusName()}}</td>
		  	<td>{{$report->last_visit}}</td>
		  	<td><a href="{{url('admin/reports/'.$report->id.'/edit')}}" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-pencil"></span></a>&nbsp;<a href="{{url('admin/reports/destroy/'.$report->id)}}" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span></a></td>
		  </tr>
		@endforeach
	  </tbody>
	</table>

@stop

@section('page_script')
<script>	
	$(document).ready(function() {
    $('#report').dataTable();
} );
</script>
@stop

