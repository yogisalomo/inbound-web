<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('CategoriesTableSeeder');
		$this->call('UsersTableSeeder');
		$this->call('ReportsTableSeeder');
	}

}

class ReportsTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('reports')->delete();
		$date = new datetime;

		DB::table('reports')->insert(array(
			array('id' => 1,'user_id' => 1,'category_id'=>1,'longitude'=>'107.6097','latitude'=>'-6.9143','description'=>'parah pak','status'=>0,'picture'=>'braga.jpg','created_at' => $date, 'updated_at' => $date),
			array('id' => 2,'user_id' => 2,'category_id'=>2,'longitude'=>'107.6096','latitude'=>'-6.9144','description'=>'Parah bgt euy','status'=>1,'picture'=>'braga.jpg', 'created_at' => $date, 'updated_at' => $date),
			array('id' => 3,'user_id' => 2,'category_id'=>3,'longitude'=>'107.6095','latitude'=>'-6.9145','description'=>'Ih Serem','status'=>2,'picture'=>'jalan.jpg', 'created_at' => $date, 'updated_at' => $date),
			array('id' => 4,'user_id' => 3,'category_id'=>4,'longitude'=>'106.6094','latitude'=>'-6.9147','description'=>'Tolong yaa','status'=>5,'picture'=>'trotoar.jpg', 'created_at' => $date, 'updated_at' => $date)
			));
	}


}

class CategoriesTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('categories')->delete();
		$date = new datetime;

		DB::table('categories')->insert(array(
			array('id' => 1,'name' => 'Jalan','description'=>'Menangani Jalan Raya', 'created_at' => $date, 'updated_at' => $date),
			array('id' => 2,'name' => 'Jembatan','description'=>'Menangani Jembatan', 'created_at' => $date, 'updated_at' => $date),
			array('id' => 3,'name' => 'Trotoar','description'=>'Menangani Trotoar', 'created_at' => $date, 'updated_at' => $date),
			array('id' => 4,'name' => 'Spanduk','description'=>'Menangani Spanduk', 'created_at' => $date, 'updated_at' => $date)
			));
	}


}

class UsersTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('users')->delete();
		$date = new datetime;

		DB::table('users')->insert(array(
			array('id' => 5,'username' => 'test','password'=>Hash::make('test'),'picture'=>'test.jpg','name'=>'Test','address_street'=>'Test','role_id'=>1, 'email'=>'test@suitcommerce.com','phone_number'=>'08117304245','registration_date'=>new datetime('2012-01-01'),'is_active'=>true, 'created_at' => $date, 'updated_at' => $date),
			array('id' => 1,'username' => 'yogisalomo','password'=>Hash::make('1234'),'picture'=>'yogi.jpg','name'=>'Yogi','address_street'=>'Test','role_id'=>1, 'email'=>'mantap@inbound.com','phone_number'=>'08117301245','registration_date'=>new datetime('2012-01-01'),'is_active'=>true, 'created_at' => $date, 'updated_at' => $date),
			array('id' => 2,'username' => 'haritselfahmi','password'=>Hash::make('2345'),'picture'=>'sahe.jpg','name'=>'Harits','address_street'=>'Test','role_id'=>2, 'email'=>'imba@inbound.com','phone_number'=>'08117304225','registration_date'=>new datetime('2012-01-01'),'is_active'=>true, 'created_at' => $date, 'updated_at' => $date),
			array('id' => 3,'username' => 'sonnylazuardi','password'=>Hash::make('3456'),'picture'=>'sonny.jpg','name'=>'Sonny','address_street'=>'Test','role_id'=>3, 'email'=>'keren@inbound.com','phone_number'=>'08117304215','registration_date'=>new datetime('2012-01-01'),'is_active'=>true, 'created_at' => $date, 'updated_at' => $date),
			array('id' => 4,'username' => 'fathanpranaya','password'=>Hash::make('4567'),'picture'=>'fathan.jpg','name'=>'Fathan','address_street'=>'Test','role_id'=>3, 'email'=>'yoi@inbound.com','phone_number'=>'08117304235','registration_date'=>new datetime('2012-01-01'),'is_active'=>false, 'created_at' => $date, 'updated_at' => $date)
			));
	}


}