<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends GenericModel implements UserInterface,RemindableInterface{

	use UserTrait, RemindableTrait;

	public $table = 'users';

	public $fillable = [
		'username',
		'name',
		'nip',
		'facebook_id',
		'flag',
		'address_street',
		'phone_number',
		'email',
		'last_visit',
		'role_id',
		'is_active',
		'point'
	];

	public $rules = [
		'username'=> 'required|unique:users',
		'name'=>'required',
		'email'=> 'email|unique:users',
	];

	protected $hidden = array('remember_token');


	public function updateLastvisit(){
		$date = new datetime;
		$date->setTimezone(new DateTimeZone('Asia/Jakarta'));
		$this->last_visit = $date;
		return true;
	}

}
