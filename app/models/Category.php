<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Category extends GenericModel2 {

	public $table = 'categories';

	public $fillable = [
		'name',
		'description',
		'picture'
	];

	public $rules = [
		'name'=>'required',
		'picture'=>'file'
	];

	
}
