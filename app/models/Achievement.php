<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Achievement extends GenericModel2 {

	public $table = 'achievements';

	public $fillable = [
		'name',
		'description',		
		'num_report',
		'num_processed',
		'picture'
	];

	public $rules = [
		'name'=>'required',
		'description'=>'required',
		'picture'=>'file'
	];

	
}
