<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Report extends GenericModel2 {

	public $table = 'reports';

	public $fillable = [
		'user_id',
		'category_id',
		'longitude',
		'latitutde',
		'altitude',
		'description',
		'status',
		'picture'
	];

	public $rules = [
		'picture'=>'file'
	];

	public function getStatusName(){
		$name= "";
		$status = $this->status;
		if($status==0){
			$name = "Belum Direspon";
		}
		else if($status==1){
			$name = "Ditunda";
		}
		else if($status==2){
			$name = "Sedang Dikerjakan";
		}
		else if($status==3){
			$name = "Selesai";
		}
		else if($status==4){
			$name = "Tidak Layak";
		}
		else if($status==5){
			$name = "Spam";
		}
		return $name;
	}
}
