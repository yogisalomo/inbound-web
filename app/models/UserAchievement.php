<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class UserAchievement extends GenericModel2 {

	public $table = 'user_achievements';

	public $fillable = [
		'user_id',
		'achievement_id'
	];

	public $rules = [
		'user_id'=>'required',
		'achievement_id'=>'required'
	];

	
}
